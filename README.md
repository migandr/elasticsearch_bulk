# ElasticSearchHelper

### :mag_right: Description

The current tool provides the basic functionalities that I have used along my career with ElasticSearch. They have a
small dataset just to make easy tests.

Note that you have to configure your own elasticsearch cluster, this helper is created for a secured authentication
with a version previous to 7.0, in case that you want to use a version greater to this, check elasticsearch official
documentation to review the minor changes that could affect your execution.

### :rocket: Execution

Once you have created, configured and reviewed all files, you can execute this tool to make a first test by:

```bash
  
  python3 -m ElasticSearchHelper
  
```

### :fire: Dependencies

All the dependencies can be installed via `pip3 install -r requirements.txt`, anyway, here you
have a detailed list:

* **elasticearch** `v6.8.1` install via `pip3 install elasticearch`
* **pandas** `v 1.0.3` install via `pip3 install pandas`


### :sweat_smile: About the developer

This tool has been developed by Miguel Andreu.

I'm an spanish Software Developer and Data Engineer, currently Studying the MSC in Big Data,
Data Science and Data Engineering at "Univerdad Autónoma de Madrid". 

:iphone: (Teams) m.andreunieva@gmail.com (Miguel Andreu Nieva)

:email: m.andreunieva@gmail.com

If you have any issue or want to add any enhancement, please use the **issues tab** to request them. 

