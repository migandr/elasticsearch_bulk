# !/usr/bin/Python3.6
import pandas as pd


class SentimentExtractor:

    def __init__(self):
        self.file = "sent_analysis/all-data.csv"
        self.content = self.normalize_file()

    def normalize_file(self):
        file = pd.read_csv(self.file, sep=",")
        return file.to_dict(orient='records')


class Sentiment(object):

    def __init__(self, sentiment):
        self.feeling = sentiment["feeling"]
        self.text = sentiment['text']

    def to_esi(self):
        state = self.__dict__.copy()
        return state
