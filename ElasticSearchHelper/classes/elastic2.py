# !usr/bin/Python3.6

from _datetime import datetime
from ElasticSearchHelper.helpers import helpers
import elasticsearch
from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk
import json
import pytz


class Elastic2(object):

    def __init__(self):
        self.config_file = helpers.open_configuration_file("configuration/config.json")
        self.index = self.config_file["index"]
        self.es = Elasticsearch([{
            "host": self.config_file["host"],
            "port": self.config_file["port"],
            "http_auth": self.__set_user_password__(),
            "url_prefix": self.config_file["url_prefix"]
        }])
        self.ingested_date = self.__set_ingested_date__()

        if self.index not in self.get_indices():
            self.__create_mapping__()

    def __set_user_password__(self) -> str:
        """

        Concatenates the user and password provided at the config file to meet the format required for the
        http_auth format.

        :return: (str) user:password

        """
        return str(self.config_file["user"]) + ":" + self.config_file["password"]

    def search(self, body):
        """
        Performs, and returns, the result of a search query.

        :param body: (dict) body of the query.

        :return: (dict) the result of the query.
        """
        return self.es.search(self.index, body=body)

    def __create_mapping__(self, request_timeout: int = 30):
        """

        Creates a mapping with the information configured on the configuration file.

        :param request_timeout: (int) (30) number of seconds for timeout

        """
        self.es.indices.create(index=self.index,
                               body=self.config_file["_mapping"],
                               request_timeout=request_timeout)

    def get_indice(self, index: str) -> Elasticsearch.index:
        """

        Try to find the required indice, if it does not exists an error is raised

        :param index: (str) Requested index

        :return: (Elasticsearch.index) -> Index if exists

        """
        try:
            return self.es.indices.get(index)
        except elasticsearch.ElasticsearchException as ex1:
            raise ex1

    def get_indices(self):
        """

        Query for the list of index in the client.

        :return: (list) names of the existing indices.

        """
        return self.es.indices.get('*')

    def es_put_mapping(self, body, doc_type):
        """
        Put a new field in the mapping of the index defined in the configuration file using the following configuration
        for the body:

        {
            "properties": {
                "tcOutputDialog": {
                    "type": "text"
                }
            }
        }

        :param body: a json with the previous information
        :param doc_type: the name of the doc type, in this example should be "item"

        :return: print the result
        """
        print(self.es.indices.put_mapping(doc_type=doc_type,
                                          index=self.config_file["index"],
                                          body=body))

    def _wrapper(self, elem):
        """
        Prepares the object that will be ingested in the elasasticsearch index using a object of the type CapsuleInfo
        and the ingested_date, the type of the object and the target index.

        :param elem: Object.

        :return: (ElasticSearch) Object.
        """
        a = dict(elem.to_esi())
        a["_op_type"] = "index"
        a["_index"] = self.index
        a["ingested_date"] = self.ingested_date
        return a

    def put_record(self, doc_type, **kwargs):
        """

        Put a single record into the wrapper index.

        :param doc_type: (str) the name of the docType
        :param kwargs: as many arguments as required.

        """
        self.es.index(index=self.index,
                      doc_type=doc_type,
                      body=json.dumps(self.__set_body__(**kwargs)))

    def __set_body__(self, **kwargs) -> dict:
        """

        For a given number of kwargs a dictionary is created with the current timestamp as first element.

        :param kwargs: as many elements as required.

        :return: (dict) a dict ready to be converted into a json

        """
        temp_dict = {"timestamp": self.ingested_date}
        temp_dict.update({k: v for (k, v) in kwargs.items()})
        return temp_dict

    def upload_data(self, data: list):
        """
        Performs the ElasticSearch bulk operation.

        :param data: (list) A list of CapsuleInfo Objects

        """
        bulk(self.es, (self._wrapper(elem) for elem in data))

    @staticmethod
    def __set_ingested_date__() -> str:
        """

        Prepares a timestamp date for the UTC zone

        :return: (str) timestamp
        """
        return str(datetime.now(pytz.utc).timestamp()).split(".")[0]
