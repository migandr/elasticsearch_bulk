# !usr/bin/Python3.6
from typing import Dict
import ujson


def open_configuration_file(config_file: str) -> Dict:
    """
    Using the ujson library, opens the parameter file handling the possible exceptions in the process
    :param config_file: the file, with the complete path, to be opened
    :return: c -> json file
    """
    try:
        with open(config_file) as f:
            c = ujson.load(f)
            return c
    except OSError:
        print("Something went wrong while opening the file")


def bulk_wrapper(data):  # pragma: no cover
    """
    Generator function that allows the data ingestion into elasticsearch
    :param data: a row of data from a json in elasticsearch format
    :return:
    """
    for elem in data:
        yield elem
