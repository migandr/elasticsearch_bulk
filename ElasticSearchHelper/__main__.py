# !usr/bin/Python3.6
from ElasticSearchHelper.classes import elastic2
from ElasticSearchHelper.classes import sentiment
from ElasticSearchHelper.helpers import helpers


if __name__ == "__main__":
    es2 = elastic2.Elastic2()
    sent = sentiment.SentimentExtractor()
    sentiments = [sentiment.Sentiment(elem) for elem in sent.content]
    es2.upload_data(helpers.bulk_wrapper(sentiments))
